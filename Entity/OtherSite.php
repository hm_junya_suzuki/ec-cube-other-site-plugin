<?php

namespace Plugin\OtherSite\Entity;

class OtherSite extends \Eccube\Entity\AbstractEntity
{
	private $product_id;
	private $rakuten_url;
	private $yahoo_url;
	private $amazon_url;
	private $dena_url;
	private $ponpare_url;
    private $Product;

    public function getRakutenUrl()
    {
        return $this->rakuten_url;
    }

    public function setRakutenUrl($rakuten)
    {
        $this->rakuten_url = $rakuten;
        return $this;
    }

    public function getYahooUrl()
    {
        return $this->yahoo_url;
    }

    public function setYahooUrl($yahoo)
    {
        $this->yahoo_url = $yahoo;
        return $this;
    }

    public function getAmazonUrl()
    {
        return $this->amazon_url;
    }

    public function setAmazonUrl($amazon)
    {
        $this->amazon_url = $amazon;
        return $this;
    }

    public function getDenaUrl()
    {
        return $this->dena_url;
    }

    public function setDenaUrl($dena)
    {
        $this->dena_url = $dena;
        return $this;
    }


    public function getPonpareUrl()
    {
        return $this->ponpare_url;
    }

    public function setPonpareUrl($ponpare)
    {
        $this->ponpare_url = $ponpare;
        return $this;
    }


    public function getProduct()
    {
        return $this->Product;
    }
    

    public function setProduct(\Eccube\Entity\Product $Product)
    {
        $this->Product = $Product;

        return $this;
    }

    public function getProductId()
    {
        return $this->product_id;
    }

    public function setProductId($productId)
    {
        $this->product_id = $productId;

        return $this;
    }

}