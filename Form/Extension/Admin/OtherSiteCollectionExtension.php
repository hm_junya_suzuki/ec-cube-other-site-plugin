<?php

namespace Plugin\OtherSite\Form\Extension\Admin;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class OtherSiteCollectionExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('rakuten_url', 'text', array(
                'label' => '楽天市場URL',
                'mapped' => false,
            ))
            ->add('yahoo_url', 'text', array(
                'label' => 'YahooショッピングURL',
                'mapped' => false,
            ))
            ->add('amazon_url', 'text', array(
                'label' => 'AmazonURL',
                'mapped' => false,
            ))
            ->add('dena_url', 'text', array(
                'label' => 'DeNAショッピングURL',
                'mapped' => false,
            ))
            ->add('ponpare_url', 'text', array(
                'label' => 'ポンパレモールURL',
                'mapped' => false,
            ))
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
    }

    public function getExtendedType()
    {
        return 'admin_product';
    }

}
