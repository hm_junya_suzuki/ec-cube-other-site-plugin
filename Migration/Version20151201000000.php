<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20151201000000 extends AbstractMigration
{

    public function up(Schema $schema)
    {
        $this->createOtherSiteTable($schema);
    }

    public function down(Schema $schema)
    {
        $schema->dropTable('plg_other_site');
    }

    protected function createOtherSiteTable(Schema $schema)
    {
        $table = $schema->createTable("plg_other_site");
        $table->addColumn('product_id', 'integer');
        $option=array('length' => 255,'notnull' => false);
        $table->addColumn('rakuten_url', 'string', $option);
        $table->addColumn('yahoo_url', 'string', $option);
        $table->addColumn('amazon_url', 'string', $option);
        $table->addColumn('dena_url', 'string', $option);
        $table->addColumn('ponpare_url', 'string', $option);
        $table->setPrimaryKey(array('product_id'));
    }
}
