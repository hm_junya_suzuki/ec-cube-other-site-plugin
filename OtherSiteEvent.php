<?php

namespace Plugin\OtherSite;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class OtherSiteEvent
{
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function showOtherSite(FilterResponseEvent $event)
    {
        $app = $this->app;

        $id = $app['request']->attributes->get('id');
        $Product = $app['eccube.repository.product']->find($id);
        $OtherSite = $app['eccube.plugin.repository.other_site']->find($id);

        if (count($OtherSite) > 0) {
            $twig = $app->renderView(
                'OtherSite/Resource/template/Front/other_site.twig',
                array(
                    'OtherSite' => $OtherSite,
                )
            );

            $response = $event->getResponse();
            $html = $response->getContent();
            $crawler = new Crawler($html);
            $oldElement = $crawler->filter('.cart_area .btn_area');
            $oldHtml = $oldElement->html();
            $newHtml = $oldHtml.$twig;

            $html = $crawler->html();
            $html = str_replace($oldHtml, $newHtml, $html);

            $response->setContent($html);
            $event->setResponse($response);
        }
    }

    public function registerOtherSite(FilterResponseEvent $event)
    {

        $app = $this->app;
        $id = $app['request']->attributes->get('id');

        $form = $app['form.factory']->createBuilder('admin_product')->getForm();

		$OtherSite=$app['eccube.plugin.repository.other_site']->find($id);
					
		if (is_null($OtherSite)) {
  			$OtherSite = new \Plugin\OtherSite\Entity\OtherSite();
		}
    	$form->get('rakuten_url')->setData($OtherSite->getRakutenUrl());
    	$form->get('yahoo_url')->setData($OtherSite->getYahooUrl());
    	$form->get('amazon_url')->setData($OtherSite->getAmazonUrl());
    	$form->get('dena_url')->setData($OtherSite->getDenaUrl());
    	$form->get('ponpare_url')->setData($OtherSite->getPonpareUrl());
				
	    $form->handleRequest($app['request']);		

       if ('POST' === $app['request']->getMethod()) {
            if ($form->isValid()) {
                $Product = $app['eccube.repository.product']->find($id);
                $OtherSite
                    ->setProductId($Product->getId())
                    ->setProduct($Product)
                    ->setRakutenUrl($form->get('rakuten_url')->getData())
                    ->setYahooUrl($form->get('yahoo_url')->getData())
                    ->setAmazonUrl($form->get('amazon_url')->getData())
                    ->setDenaUrl($form->get('dena_url')->getData())
                    ->setPonpareUrl($form->get('ponpare_url')->getData())
                    ;
                $app['orm.em']->persist($OtherSite);
                $app['orm.em']->flush();
            }
        }

    }

    public function addContentOnProductEdit(FilterResponseEvent $event)
    {
        $app = $this->app;
        $request = $event->getRequest();
        $response = $event->getResponse();
        $id = $request->attributes->get('id');

        $html = $response->getContent();
        $crawler = new Crawler($html);

        $form = $app['form.factory']
            ->createBuilder('admin_product')
            ->getForm();

        if ($id) {
            $Product = $app['eccube.repository.product']->find($id);
        } else {
            $Product = new \Eccube\Entity\Product();
        }

        $OtherSite = $app['eccube.plugin.repository.other_site']->findOneBy(
            array(
                'product_id' => $id,
            ));
		if (isset($OtherSite)) {
        	$form->get('rakuten_url')->setData($OtherSite->getRakutenUrl());
        	$form->get('yahoo_url')->setData($OtherSite->getYahooUrl());
        	$form->get('amazon_url')->setData($OtherSite->getAmazonUrl());
        	$form->get('dena_url')->setData($OtherSite->getDenaUrl());
        	$form->get('ponpare_url')->setData($OtherSite->getPonpareUrl());
		}
        $form->handleRequest($request);

		$twig = $app->renderView('OtherSite/Resource/template/Admin/other_site.twig',
    			array('form' => $form->createView()));

		$oldElement = $crawler
		    ->filter('.accordion')
		    ->last();

		if ($oldElement->count() > 0) {
		    $oldHtml = $oldElement->html();
    		$newHtml = $oldHtml.$twig;
    		$html = $crawler->html();
    		$html = str_replace($oldHtml, $newHtml, $html);
		    $response->setContent($html);
    		$event->setResponse($response);
		}
    }
}