<?php

namespace Plugin\OtherSite\ServiceProvider;

use Eccube\Application;
use Silex\Application as BaseApplication;
use Silex\ServiceProviderInterface;

class OtherSiteServiceProvider implements ServiceProviderInterface
{
    public function register(BaseApplication $app)
    {

        $app['form.type.extensions'] = $app->share($app->extend('form.type.extensions', function ($extensions) use ($app) {
            $extensions[] = new \Plugin\OtherSite\Form\Extension\Admin\OtherSiteCollectionExtension();

            return $extensions;
        }));

        $app['eccube.plugin.repository.other_site'] = function () use ($app) {
            return $app['orm.em']->getRepository('\Plugin\OtherSite\Entity\OtherSite');
        };

    }

    public function boot(BaseApplication $app)
    {
    }
}